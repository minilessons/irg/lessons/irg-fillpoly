function vect2(x,y) {
  this.x = x;
  this.y = y;
}
vect2.prototype.add = function(other) {
  return new vect2(this.x+other.x, this.y+other.y);
}
vect2.prototype.norm = function() {
  return Math.sqrt(this.x*this.x + this.y*this.y);
}
vect2.prototype.normalize = function() {
  var n = this.norm();
  return new vect2(this.x/n, this.y/n);
}
vect2.prototype.scaled = function(d) {
  return new vect2(this.x*d, this.y*d);
}

function RasterDisplay(canvasID, width, height) {
  this.width = width;
  this.height = height;
  this.canvas = document.getElementById(canvasID);
  this.context = this.canvas.getContext("2d");
  var regions = [];
  var ctx = this.context;
  this.mathematical = false;
  this.showCoordinates = true;
  this.backgroundColor = "#FFFFFF";
  this.foregroundColor = "#0000FF";
  this.gridColor = "#000000";
  this.showPoints = false;
  this.pointsColor = "#FF0000";
  this.points = null;
  this.showPointLabels = false;
  this.showLineLabels = false;

  this.pixels = [];
  for(let r = 0; r < height; r++) {
    let row = [];
    for(let c = 0; c < width; c++) {
      row.push(0);
    }
    this.pixels.push(row);
  }

  this.clear = function() {
    for(let r = 0; r < height; r++) {
      for(let c = 0; c < width; c++) {
        this.pixels[r][c] = 0;
      }
    }
  }

  this.paint = function() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.fillStyle = "#ffffee";
    this.context.strokeStyle = "#aaaa00";

    regions.length = 0;

    let marginHorizontal = 20;
    let marginVertical = 20;

    let dispX = marginHorizontal;
    let dispW = this.canvas.width - marginHorizontal;
    let dispY = this.mathematical ? 0 : marginVertical;
    let dispH = this.canvas.height - marginVertical;

    let pxwidth = dispW / this.width;
    let pxheight = dispH / this.height;
    for(let y = 0; y < this.height; y++) {
      for(let x = 0; x < this.width; x++) {
        let xa = x;
        let ya = this.mathematical ? this.height-1-y : y;
        let xs = pxwidth * xa;
        let xe = pxwidth * (xa+1);
        let ys = pxheight * ya;
        let ye = pxheight * (ya+1);
        this.context.fillStyle = this.pixels[y][x]==0 ? this.backgroundColor : this.foregroundColor;
        this.context.fillRect(Math.round(xs)+dispX,Math.round(ys)+dispY,Math.round(xe-xs),Math.round(ye-ys));
        regions.push({x1: xs+dispX, y1: ys+dispY, x2: xe+dispX, y2: ye+dispY, x: x, y: y});
      }
    }

    this.context.strokeStyle = this.gridColor;
    for(let y = 0; y <= this.height; y++) {
      let yline = y * (dispH-1)/this.height;
      this.context.beginPath();
      this.context.moveTo(dispX+0,dispY+yline);
      this.context.lineTo(dispX+dispW, dispY+yline);
      this.context.stroke();
    }
    for(let x = 0; x <= this.width; x++) {
      let xline = x * (dispW-1)/this.width;
      this.context.beginPath();
      this.context.moveTo(dispX+xline,dispY+0);
      this.context.lineTo(dispX+xline,dispY+dispH);
      this.context.stroke();
    }

    if(this.showCoordinates) {
      this.context.fillStyle = "#000000";
      this.context.font = '10px sans';
      this.context.textAlign = 'right';      
      this.context.textBaseline='middle';
      
      for(let y = 0; y < this.height; y++) {
        let yline = (this.mathematical ? this.height-1-y : y) * dispH/this.height + dispH/this.height/2.0;
        this.context.fillText(y, marginHorizontal-3, this.mathematical ? yline : yline + marginVertical);
      }

      this.context.textAlign = 'center';      
      this.context.textBaseline=this.mathematical ? 'top' : 'bottom';
      for(let x = 0; x < this.width; x++) {
        let xline = x * dispW/this.width + dispW/this.width/2.0;
        this.context.fillText(x, marginHorizontal+xline, this.mathematical ? dispH+3 : marginVertical-3);
      }

      if(this.showPoints && this.points!=null) {
        this.context.fillStyle = this.pointsColor;
        this.context.font = '16px sans';
        this.context.textAlign = 'center';      
        this.context.textBaseline='middle';
        for(let i = 0; i < this.points.length; i++) {
          let pt = this.points[i];
          let xl0 = pt.x * dispW/this.width + dispW/this.width/2.0;
          let yl0 = (this.mathematical ? this.height-1-pt.y : pt.y) * dispH/this.height + dispH/this.height/2.0;
          this.context.beginPath();
          this.context.arc(dispX+xl0,dispY+yl0,5,0,2*Math.PI);
          this.context.fill();
          if(this.showPointLabels && pt.l) {
            var previ = (i+this.points.length-1)%this.points.length;
            var nexti = (i+this.points.length+1)%this.points.length;
            console.log(previ);
            console.log(nexti);

            let ppx = dispX + this.points[previ].x * dispW/this.width + dispW/this.width/2.0;
            let ppy = dispY + (this.mathematical ? this.height-1-this.points[previ].y : this.points[previ].y) * dispH/this.height + dispH/this.height/2.0;
            let pcx = dispX + this.points[i].x * dispW/this.width + dispW/this.width/2.0;
            let pcy = dispY + (this.mathematical ? this.height-1-this.points[i].y : this.points[i].y) * dispH/this.height + dispH/this.height/2.0;
            let pnx = dispX + this.points[nexti].x * dispW/this.width + dispW/this.width/2.0;
            let pny = dispY + (this.mathematical ? this.height-1-this.points[nexti].y : this.points[nexti].y) * dispH/this.height + dispH/this.height/2.0;

            var v1 = new vect2(pcx-ppx,pcy-ppy);
            var v2 = new vect2(pcx-pnx,pcy-pny);
            console.log("Gledam za i="+i);
            console.log(v1.x+", "+v1.y);
            console.log(v2.x+", "+v2.y);
            var vectOffset = v1.normalize().add(v2.normalize()).normalize().scaled(20);
            console.log((pcx+vectOffset.x)+", "+(pcy+vectOffset.y));
            this.context.fillText(pt.l, pcx+vectOffset.x, pcy+vectOffset.y);
          }
        }
      }

      if(this.vlines!=null) {
        for(let i = 0; i < this.vlines.length; i++) {
          let l = this.vlines[i];
          let ld = this.context.getLineDash();
          this.context.strokeStyle = l.c;
          this.context.setLineDash(l.d);
          let xl0 = l.x * dispW/this.width + dispW/this.width/2.0;
          let yl0 = (this.mathematical ? this.height-1 : 0) * dispH/this.height + dispH/this.height/2.0;
          let yl1 = (this.mathematical ? 0 : this.height-1) * dispH/this.height + dispH/this.height/2.0;
          this.context.beginPath();
          this.context.moveTo(dispX+xl0,dispY+yl0);
          this.context.lineTo(dispX+xl0,dispY+yl1);
          this.context.stroke();
          this.context.setLineDash(ld);
        }
      }

      if(this.hlines!=null) {
        for(let i = 0; i < this.hlines.length; i++) {
          let l = this.hlines[i];
          let ld = this.context.getLineDash();
          this.context.strokeStyle = l.c;
          this.context.setLineDash(l.d);
          let xl0 = 0 * dispW/this.width + dispW/this.width/2.0;
          let xl1 = (this.width-1) * dispW/this.width + dispW/this.width/2.0;
          let yl0 = (this.mathematical ? this.height-1-l.y : l.y) * dispH/this.height + dispH/this.height/2.0;
          this.context.beginPath();
          this.context.moveTo(dispX+xl0,dispY+yl0);
          this.context.lineTo(dispX+xl1,dispY+yl0);
          this.context.stroke();
          this.context.setLineDash(ld);
        }
      }

      if(this.fullLines && this.fullLines.length>0) {
        for(let i = 0; i < this.fullLines.length; i++) {
          let l = this.fullLines[i];
          let xl0 = l.x0 * dispW/this.width + dispW/this.width/2.0;
          let yl0 = (this.mathematical ? this.height-1-l.y0 : l.y0) * dispH/this.height + dispH/this.height/2.0;
          let xl1 = l.x1 * dispW/this.width + dispW/this.width/2.0;
          let yl1 = (this.mathematical ? this.height-1-l.y1 : l.y1) * dispH/this.height + dispH/this.height/2.0;
          this.context.strokeStyle = l.c;
          this.context.beginPath();
          this.context.moveTo(dispX+xl0,dispY+yl0);
          this.context.lineTo(dispX+xl1,dispY+yl1);
          this.context.stroke();
          if(this.showLineLabels && l.l) {
            this.context.fillStyle = l.c;
            this.context.font = '16px sans';
            this.context.textAlign = 'center';
            this.context.textBaseline='middle';
            var txl0 = dispX+xl0;
            var tyl0 = dispY+yl0;
            var txl1 = dispX+xl1;
            var tyl1 = dispY+yl1;
            var normal = new vect2(tyl1-tyl0, txl0-txl1).normalize();
            var midpoint = new vect2((txl0+txl1)/2,(tyl0+tyl1)/2).add(normal.scaled(20));
            this.context.fillText(l.l, midpoint.x, midpoint.y);
          }
        }
      }

      if(this.xpoints && this.xpoints.length > 0) {
        for(let i = 0; i < this.xpoints.length; i++) {
          let l = this.xpoints[i];
          let xl0 = l.x * dispW/this.width + dispW/this.width/2.0;
          let yl0 = (this.mathematical ? this.height-1-l.y : l.y) * dispH/this.height + dispH/this.height/2.0;
          this.context.fillStyle = "#000000";
          this.context.beginPath();
          this.context.rect(dispX+xl0-5,dispY+yl0-5, 10, 10);
          this.context.fill();
        }
      }
    }
  }

  var me = this;

  this.canvas.addEventListener('mousedown', function(evt) {
    var rect = me.canvas.getBoundingClientRect();
    var pos = {x: evt.clientX - rect.left, y: evt.clientY - rect.top};
    var selected = null;
    for(var i = 0; i < regions.length; i++) {
      var r = regions[i];
      if(pos.x < r.x1 || pos.x > r.x2 || pos.y < r.y1 || pos.y > r.y2) continue;
      selected = r;
      break;
    }
    if(selected!=null) {
      me.paint();
      //alert("Piksel "+selected.x+", "+selected.y);
    }
  });

  //this.paint();
}

function FillAlgorithm1(vertexes, gridDisplay, prefix) {
  this.update1 = function() {
    if(!prefix) return;
    let v = document.getElementById(prefix+"_xmin");
    if(v) v.innerHTML = this.minX;
    v = document.getElementById(prefix+"_ymin");
    if(v) v.innerHTML = this.minY;
    v = document.getElementById(prefix+"_xmax");
    if(v) v.innerHTML = this.maxX;
    v = document.getElementById(prefix+"_ymax");
    if(v) v.innerHTML = this.maxY;
    v = document.getElementById(prefix+"_yscan");
    if(v) v.innerHTML = this.hasOwnProperty('scanY') ? this.scanY : "?";
    v = document.getElementById(prefix+"_varL");
    if(v) v.innerHTML = this.hasOwnProperty('varL') ? (this.varL ? this.varL.toFixed(3) : this.varL) : "?";
    v = document.getElementById(prefix+"_varD");
    if(v) v.innerHTML = this.hasOwnProperty('varD') ? (this.varD ? this.varD.toFixed(3) : this.varD) : "?";
  }

  this.reset = function() {
    this.minX = "?";
    this.maxX = "?";
    this.minY = "?";
    this.maxY = "?";
    this.scanY = "?";
    this.update1();

    this.minX = vertexes[0].x;
    this.maxX = vertexes[0].x;
    this.minY = vertexes[0].y;
    this.maxY = vertexes[0].y;
    for(let i = 0; i < vertexes.length; i++) {
      let pt = vertexes[i];
      if(this.maxX < pt.x) this.maxX = pt.x;
      if(this.minX > pt.x) this.minX = pt.x;
      if(this.maxY < pt.y) this.maxY = pt.y;
      if(this.minY > pt.y) this.minY = pt.y;
    }
    this.edges = [];
    for(let i = 0; i < vertexes.length; i++) {
      let pt1 = vertexes[i];
      let pt2 = vertexes[(i+1) % vertexes.length];
      let koef = [pt1.y-pt2.y, pt2.x-pt1.x, pt1.x*pt2.y-pt1.y*pt2.x];
      this.edges.push({pt1: pt1, pt2: pt2, koef: koef, isLeft: pt1.y>pt2.y, isHorizontal: pt1.y==pt2.y});
    }
    this.vlines = [];
    this.hlines = [];
    this.stanje = "INIT";
    this.scanY = this.minY;
    gridDisplay.vlines = this.vlines;
    gridDisplay.hlines = this.hlines;
    gridDisplay.fullLines = [];
    gridDisplay.xpoints = [];
    gridDisplay.clear();
    gridDisplay.paint();
  }

  this.reset();

  this.hasNext = function() {
    return this.stanje != "END";
  }
  this.next = function() {
    if(!this.hasNext()) return;
    if(this.stanje=="INIT") {
      delete this.scanY;
      this.stanje = "SLINE";
      this.vlines.push({x: this.minX, c: "#FF0000", d: [5,5]});
      this.vlines.push({x: this.maxX, c: "#FF0000", d: [5,5]});
      this.hlines.push({y: this.minY, c: "#FF0000", d: [5,5]});
      this.hlines.push({y: this.maxY, c: "#FF0000", d: [5,5]});
      gridDisplay.paint();
      this.update1();
      this.hlines.length = 0;
      this.vlines.length = 0;
      return;
    } else if(this.stanje=="SLINE") {
      this.varL = this.minX;
      this.varD = this.maxX;
      if(this.hasOwnProperty('scanY')) {
        if(this.scanY+1>this.maxY) {
          this.stanje = "END";
          gridDisplay.paint();
          return;
        }
        this.scanY++;
      } else {
        this.scanY = this.minY;
      }
      this.hlines.push({y: this.scanY, c: "#0000FF", d: [5,5]});
      this.vlines.push({x: this.varL, c: "#006666", d: [4,2]});
      this.vlines.push({x: this.varD, c: "#FF0000", d: [4,2]});
      this.stanje = "SNE";
      delete this.currentEdgeIndex;
      gridDisplay.paint();
      this.update1();
      return;
    } else if(this.stanje=="SNE") {
      if(this.hasOwnProperty('currentEdgeIndex')) {
        this.currentEdgeIndex++;
      } else {
        this.currentEdgeIndex = 0;
      }
      let e = this.edges[this.currentEdgeIndex];
      if(!e.isHorizontal) {
        let xsjec = (-e.koef[1]*this.scanY-e.koef[2])/e.koef[0];
        gridDisplay.xpoints = [{x: xsjec, y: this.scanY}];
      }
      gridDisplay.fullLines.length = 0;
      gridDisplay.fullLines.push({x0:e.pt1.x, y0:e.pt1.y, x1:e.pt2.x, y1:e.pt2.y, c: e.isLeft ? "#006666" : "#FF0000"});
      this.stanje="ULR";
      gridDisplay.paint();
      this.update1();
      return;
    } else if(this.stanje=="ULR") {
      let e = this.edges[this.currentEdgeIndex];
      if(!e.isHorizontal) {
        let xsjec = (-e.koef[1]*this.scanY-e.koef[2])/e.koef[0];
        if(e.isLeft && xsjec > this.varL) {
          this.varL = xsjec;
          this.vlines[this.vlines.length-2].x = xsjec;
        }
        if(!e.isLeft && xsjec < this.varD) {
          this.varD = xsjec;
          this.vlines[this.vlines.length-1].x = xsjec;
        }
      }
      gridDisplay.paint();
      this.update1();
      gridDisplay.xpoints = [];
      if(this.currentEdgeIndex+1<this.edges.length) {
        this.stanje="SNE";
      } else {
        this.stanje="FLINE";
      }
      return;
    } else if(this.stanje=="FLINE") {
      this.hlines.pop();
      this.vlines.pop();
      this.vlines.pop();
      if(this.varL <= this.varD) {
        let st = Math.round(this.varL);
        let en = Math.round(this.varD);
        for(let j = st; j <= en; j++) {
          gridDisplay.pixels[this.scanY][j] = 1;
        }
      }
      gridDisplay.fullLines.length = 0;
      gridDisplay.paint();
      if(this.scanY+1 <= this.maxY) {
        this.stanje="SLINE";
      } else {
        this.stanje="END";
      }
    }
  }
}

function parseLineData(prefixID, vertexes, w, h) {
  let v = document.getElementById(prefixID).value;
  if(v) v = new String(v).trim();
  if(!v) { alert("Neispravan unos."); return false; }
  let a = v.split(",");
  if((a.length % 2) != 0 || a.length<6) {
    alert("Moraju biti zadane barem tri točke."); return false;
  }
  let n = a.length/2;
  let narr = [];
  for(let i = 0; i < n; i++) {
    let v1 = parseInt(a[i*2].trim());
    let v2 = parseInt(a[i*2+1].trim());
    if(isNaN(a[i*2].trim()) || isNaN(a[i*2+1].trim())) {
      alert("Neispravan unos: konverzija u broj nije moguća.");
      return false;
    }
    narr.push({x: v1, y: v2, l:"V"+(i+1)});
  }
  vertexes.length = 0;
  for(let i = 0; i < narr.length; i++) {
    vertexes.push(narr[i]);
  }
  return true;
}

