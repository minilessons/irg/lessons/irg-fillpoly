#!/bin/bash

if [ ! -d package ]; then
    echo "Directory package does not exists."
    exit
fi

cd package

if [ $? -ne 0 ]; then
    echo "Could not cd into package directory."
    exit
fi

zip -r9 ../package.zip *

cd ..

if [ ! -d video ]; then
  echo "Video directory not found. Skipping video archive creation..."
  exit 1
fi

cd video
zip -0 ../video.zip *
echo "Created video.zip."
